# KVP Form

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Specifications

KvpFormComponent is an component for modifying Angular FormGroups. Modify KvpFormComponent to add the following features:
1. For each FormControl in the FormGroup, display  
    * label with the FormControl name
    * input box for editing the FormControl value
    * delete button for removing the FormControl
2. Allow the user to add additional FormControl to the FormGroup
