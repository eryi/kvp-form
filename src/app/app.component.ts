import { Component } from '@angular/core'
import { FormBuilder } from '@angular/forms'

@Component({
  selector: 'tc-app',
  template: `
    <h2>Editor</h2>
    <form [formGroup]="form">
      <tc-kvp-form formGroupName="group"></tc-kvp-form>
      <input type="text" formControlName="control">
    </form>
    <h2>Value</h2>
    <pre>
{{form.value | json}}
    </pre>
  `,
})
export class AppComponent  {
  form = this.fb.group({
    group: this.fb.group({
      key1: 'value1',
      key2: 'value2',
    }),
    control: 'control-value'
  })

  constructor(private fb: FormBuilder) {}
}